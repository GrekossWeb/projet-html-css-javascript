// VARIABLES
let pizzaContainer = document.body.querySelector('#pizzaContainer')

let pizzas = [
    { name: 'Pizza Margherita', imgUrl: 'assets/img/pizza1.jpg' },
    { name: 'Pizza aux poivrons', imgUrl: 'assets/img/pizza2.jpg' },
    { name: 'Pizza aux champignons', imgUrl: 'assets/img/pizza3.jpg' },
    { name: 'Pizza aux fruits de mer', imgUrl: 'assets/img/pizza4.jpg' },
    { name: 'Pizza hawaiienne', imgUrl: 'assets/img/pizza5.jpg' },
    { name: 'Pizza Margherita', imgUrl: 'assets/img/pizza1.jpg' },
    { name: 'Pizza aux poivrons', imgUrl: 'assets/img/pizza2.jpg' },
    { name: 'Pizza aux champignons', imgUrl: 'assets/img/pizza3.jpg' },
    { name: 'Pizza aux fruits de mer', imgUrl: 'assets/img/pizza4.jpg' },
    { name: 'Pizza hawaiienne', imgUrl: 'assets/img/pizza5.jpg' },
    { name: 'Pizza Margherita', imgUrl: 'assets/img/pizza1.jpg' },
    { name: 'Pizza aux poivrons', imgUrl: 'assets/img/pizza2.jpg' },
    { name: 'Pizza aux champignons', imgUrl: 'assets/img/pizza3.jpg' },
    { name: 'Pizza aux fruits de mer', imgUrl: 'assets/img/pizza4.jpg' },
    { name: 'Pizza hawaiienne', imgUrl: 'assets/img/pizza5.jpg' }
];



// PROGRAM
const cards = createCards(pizzas);
addCardsInHtml(cards);



// FONCTIONS

/**
 * Créer des cartes Pizza
 * @param imgTab Tableau de pizzas
 * @returns Le tableaux des cartes créées
 */
function createCards(pizzaTab) {
    let tab = [];
    // Boucle sur le tableau des pizzas pour créer les cartes en fonction du nombre de pizza
    for (let index = 0; index < pizzaTab.length; index++) {
        let card = document.createElement('div');
        card.classList.add('card');
        let imgPizza = document.createElement('img');
        imgPizza.src = pizzaTab[index].imgUrl;
        imgPizza.width = 400;
        imgPizza.height = 250;

        let pizzaName = document.createElement('h2');
        pizzaName.textContent = pizzaTab[index].name;

        let descriptionPizza = document.createElement('p');
        descriptionPizza.textContent = 'Veritatis maecenas minus quisquam curabitur dictum, fugit nascetur sociis at non sint, tempor distinctio iste.'

        card.appendChild(imgPizza); // Ajoute l'image dans la carte
        card.appendChild(pizzaName); // Ajoute le nom de la pizza dans la carte
        card.appendChild(descriptionPizza); // Ajoute la description de la pizza dans la carte
        tab.push(card); // Ajoute la carte dans le tableau de cartes
    }

    return tab;
}

/**
 * Ajoute toutes les cartes du tableau passé en paramètre dans l'élément <main>
 * @param cardsTab Tableau des cartes à ajouter
 */
function addCardsInHtml(cardsTab) {
    cardsTab.forEach(card => {
        pizzaContainer.appendChild(card);
    })
}

